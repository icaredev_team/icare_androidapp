package com.example.root.maps.service;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;


import com.example.root.maps.model.User;
import com.example.root.maps.util.DbHelper;
import com.example.root.maps.util.SessionManager;

/**
 * Created by root on 28/01/17.
 */
public class AuthenticationService extends Activity{

    private SessionManager session ;

    private static DbHelper myDbHelper ;

    public AuthenticationService(Context context) {
        myDbHelper = new DbHelper(context);
        session = new SessionManager(context);

    }

    public boolean checkLogin(String login, String password) {

        if(myDbHelper.signUp(login,password) != null){

            Log.i("test", "AuthenticationService");

            User user  = myDbHelper.signUp(login,password);
            // Activate the User's session !
            Log.i("test", user.getFirstName() + "-" + user.getLastName() + "-" + user.getEmail() + "-" + user.getPassword() + "-" + user.getPhoneNumber() );

            session.saveUserInfo(user.getFirstName(), user.getLastName(), user.getEmail(), user.getPassword(), user.getPhoneNumber(), user.isDriver());
            session.setLoggedIn(true);
            return true;
        }
        return false;
    }



}
