package com.example.root.maps;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.root.maps.model.UserMinimal;
import com.example.root.maps.model.User;
import com.example.root.maps.model.UserMinimal;
import com.example.root.maps.service.LocationGpsListener;
import com.example.root.maps.util.Constant;
import com.example.root.maps.util.MapsUtil;
import com.example.root.maps.util.NotificationsHandler;
import com.example.root.maps.util.SessionManager;
import com.example.root.maps.util.SocketProvider;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class MapsFragmentDriver extends Fragment implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private LocationListener locListener;
    private LocationManager locManager;
    private GoogleMap mMap;
    private SessionManager session;
    private LatLng myLocation;
    private LocationGpsListener gps;
    private Socket mSocket;
    private NotificationsHandler notificationsHandler;
    HashMap<String,Marker> hashMapMarker;
    MapsUtil mapsUtil;
    UserMinimal walker;
    LatLng walkerLocation;
    LatLng walkerDestination;
    String walkerLocationAdress;
    String walkerDestinationAdress;
//    BroadcastReceiver receiver;

//    HashMap<String,UserMinimal> drivers;
//    List<Driver> drivers = new ArrayList<Driver>();

    private MapFragment fragment;

    protected GoogleApiClient mGoogleApiClient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        {
            try {
                mSocket = IO.socket(Constant.CHAT_SERVER_URL);
            } catch (URISyntaxException e) {
                throw new RuntimeException(e);
            }
        }

//        mSocket.on(Socket.EVENT_CONNECT,onConnect);
//        mSocket.on(Socket.EVENT_DISCONNECT,onDisconnect);

        mSocket.on("request received", onRequestReceived);
        mSocket.on("update walker location", onUpdateLocationWalker);
//        mSocket.on("driver connected", onDriverConnected);
        mSocket.on("walker disconnected", onWalkerDisconnected);
//        mSocket.on("test", test);
        mSocket.connect();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_maps, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);




        fragment = (MapFragment)getChildFragmentManager().findFragmentById(R.id.map);
        fragment.getMapAsync(this);

        view.findViewById(R.id.btn_choose_destination).setVisibility(View.INVISIBLE);
        view.findViewById(R.id.btn_confirm_destination).setVisibility(View.INVISIBLE);
        view.findViewById(R.id.btn_abord_destination).setVisibility(View.INVISIBLE);

        hashMapMarker = new HashMap<>();

        notificationsHandler = new NotificationsHandler();

        session = new SessionManager(getActivity());

        // Setting click event handler for InfoWIndow

        if(!session.isLoggedIn())
        {
            Intent intent = new Intent(getActivity() , LoginActivity.class);
            startActivity(intent);
        }

//        receiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                if (intent.getAction().equals("com.example.root.maps.call")) {
//
//                    Log.i("test", "broadcast received call");
//                } else if (intent.getAction().equals("com.example.root.maps.decline")) {
//
//                    Log.i("test", "broadcast received decline");
//
////                    mSocket.emit("decline request" );
//
//                }
//
//            }
//        };


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {


        mMap = googleMap;
        mapsUtil = new MapsUtil(mMap, hashMapMarker);

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Intent callIntent = new Intent();
                callIntent.setAction("com.example.root.maps.call");
                callIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                callIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                getActivity().sendBroadcast(callIntent);
            }
        });



        gps = new LocationGpsListener(getActivity(), mMap)
        {
            @Override
            public void onLocationChanged(Location location) {
                super.onLocationChanged(location);
                myLocation = new LatLng(gps.getLatitude(), gps.getLongitude());
                mapsUtil.UpdateLocation(myLocation, session.getUserInfo().getEmail());
                JSONObject userJSON = new JSONObject();
                JSONObject locationJSON = new JSONObject();

                try {
                    userJSON.put("category", 2);
                    userJSON.put("username", session.getUserInfo().getEmail());
                    locationJSON.put("latitude", myLocation.latitude);
                    locationJSON.put("longitude", myLocation.longitude);
                    userJSON.put("location",locationJSON);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mSocket.emit("update location", userJSON);
            }
        };

        myLocation = new LatLng(gps.getLatitude(), gps.getLongitude());

        Log.i("test", "========" + gps.getLatitude() + "--" + gps.getLongitude());


        mapsUtil.UpdateLocation(myLocation, session.getUserInfo().getFirstName() + " " + session.getUserInfo().getLastName() , session.getUserInfo().getEmail());


//        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener(){
//
//            @Override
//            public boolean onMyLocationButtonClick() {
//
//                return true;
//            }
//        });

//        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
//
//            @Override
//            public void onInfoWindowClick(Marker marker) {
//                // Remove the marker
//
//                mSocket.emit("ask driver", marker.getTitle());
//            }
//        });



//            LatLngBounds perimeter = new LatLngBounds(new LatLng(myLocation.latitude - 0.15,
//                    myLocation.longitude - 0.15), new LatLng(myLocation.latitude + 0.15, myLocation.longitude + 0.15));
//            perimeter.contains();



        try {
            connectToServer();
        } catch (JSONException e) {
            e.printStackTrace();
        }




    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private Emitter.Listener onRequestReceived = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    JSONObject walkerJSON = (JSONObject) args[0];
                    JSONObject locationJSON;
                    JSONObject destinationJSON;

                    /////Notification


                    Geocoder geocoder;
                    List<Address> addresses;
                    geocoder = new Geocoder(getActivity(), Locale.getDefault());

                    try {

                        walker = new UserMinimal(walkerJSON.getString("name"), walkerJSON.getString("username"), walkerJSON.getString("phonenumber"));
                        locationJSON = (JSONObject) walkerJSON.getJSONObject("location");
                        destinationJSON = (JSONObject) walkerJSON.getJSONObject("destination");

                        walkerLocation = new LatLng(locationJSON.getDouble("latitude"), locationJSON.getDouble("longitude"));
                        walkerDestination = new LatLng(destinationJSON.getDouble("latitude"), destinationJSON.getDouble("longitude"));
                        addresses = geocoder.getFromLocation(walkerDestination.latitude, walkerDestination.longitude, 1);
                        walkerDestinationAdress = addresses.get(0).getAddressLine(0);  // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                        addresses = geocoder.getFromLocation(walkerLocation.latitude, walkerLocation.longitude, 1);
                        walkerLocationAdress = addresses.get(0).getAddressLine(0);



//                        mapsUtil.UpdateLocation(location, walkerJSON.getString("name"), walkerJSON.getString("username"));


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }catch (IOException e) {
                        e.printStackTrace();
                    }

                    notificationsHandler.generateNotification(getActivity(), walker, walkerLocationAdress, walkerDestinationAdress);






//                    JSONObject walkerJSON = (JSONObject) args[0];
//                    JSONObject locationJSON;
//                    LatLng location;

//                    Log.i("test", "onShowWalker");
//
//                        try {
//
//                            walker = new UserMinimal(walkerJSON.getString("name"), walkerJSON.getString("username"), walkerJSON.getString("phonenumber"));
//                            locationJSON = (JSONObject) walkerJSON.getJSONObject("location");
//
//                            location = new LatLng(locationJSON.getDouble("latitude"), locationJSON.getDouble("longitude"));
//                            mapsUtil.UpdateLocation(location, walkerJSON.getString("name"), walkerJSON.getString("username"));
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    Log.i("test", "onShowWalker2");


                }
            });
        }
    };

    private Emitter.Listener onWalkerDisconnected = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    JSONObject usernameJSON = (JSONObject) args[0];
//
                    String username;
                    try {
                        username = usernameJSON.getString("username");
                        Log.i("test", "onDriverDisconnected " + username);

                        mapsUtil.removeMarker(username);

                    } catch (JSONException e) {
                        return;
                    }
                }
            });
        }
    };

    private Emitter.Listener onUpdateLocationWalker = new Emitter.Listener() {


        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    String username;
                    JSONObject walkerJSON = (JSONObject) args[0];
                    JSONObject locationJSON;
                    LatLng location;
                    try {
                        username = walkerJSON.getString("username");
                        locationJSON = (JSONObject) walkerJSON.getJSONObject("location");

//                        Log.i("test", driverJSON.getString("username") + "///////////");

                        Log.i("test", username + "******************" );

                        location = new LatLng(locationJSON.getDouble("latitude"), locationJSON.getDouble("longitude"));
                        mapsUtil.UpdateLocation(location, username);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        mSocket.connect();
    }

//    private Emitter.Listener onDriverConnected = new Emitter.Listener() {
//        @Override
//        public void call(final Object... args) {
//            getActivity().runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//
//
//
//                    JSONObject driverJSON = (JSONObject) args[0];
//                    JSONObject locationJSON;
//                    LatLng location;
//                        try {
//                            drivers.put(driverJSON.getString("username") ,new Driver(driverJSON.getString("name"), driverJSON.getString("username"), driverJSON.getString("phonenumber")));
//                            locationJSON = (JSONObject) driverJSON.getJSONObject("location");
//
//                            location = new LatLng(locationJSON.getDouble("latitude"), locationJSON.getDouble("longitude"));
//                            addMarker(location, driverJSON.getString("name"), driverJSON.getString("username"));
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//
//                }
//            });
//        }
//    };

//    private Emitter.Listener test = new Emitter.Listener() {
//
//
//        @Override
//        public void call(final Object... args) {
//            getActivity().runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//
//                    Log.i("test", "test000");
//
////                    addMarker(new LatLng(33.1, -6.1), "testt !!");
//
////                    JSONObject locationJSON = (JSONObject) args[0];
////
////                    try {
////                        LatLng location = new LatLng(locationJSON.getDouble("latitude"), locationJSON.getDouble("longitude"));
////                        addMarker(location, "testt !!");
////                    } catch (JSONException e) {
////                        e.printStackTrace();
////                    }
//
//
//                }
//            });
//        }
//    };

//    private Emitter.Listener onConnect = new Emitter.Listener() {
//        @Override
//        public void call(final Object... args) {
//            getActivity().runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//
//
//
//
//
//
//                }
//            });
//        }
//    };

//    private Emitter.Listener onDisconnect = new Emitter.Listener() {
//        @Override
//        public void call(final Object... args) {
//            getActivity().runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//
//
//
//                }
//            });
//        }
//    };

//    private Emitter.Listener onDriverDisconnected = new Emitter.Listener() {
//        @Override
//        public void call(final Object... args) {
//            getActivity().runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//
//                    Log.i("test", "onDriverDisconnected");
//
//
//                    JSONObject usernameJSON = (JSONObject) args[0];
//
//                    String username;
//                    try {
//                        username = usernameJSON.getString("username");
//                        Log.i("test", "onDriverDisconnected " + username);
//
//                        drivers.remove(username);
//                        removeMarker(username);
//
//                    } catch (JSONException e) {
//                        return;
//                    }
//
//
//                }
//            });
//        }
//    };



    public void connectToServer() throws JSONException {

        Log.i("test", "here-----------");


        User u = session.getUserInfo();

        JSONObject user = new JSONObject();
        JSONObject location = new JSONObject();
        user.put("category", 2);
        user.put("name", u.getFirstName() + " " + u.getLastName());
        user.put("username", u.getEmail());
        user.put("phonenumber", u.getPhoneNumber());

        location.put("latitude", myLocation.latitude);
        location.put("longitude", myLocation.longitude);

        user.put("location",location);

        mSocket.emit("add driver", user);

    }


//    @Override
//    public void onRestart() {
//        super.onStart();
//        myLocation = new LatLng(gps.getLatitude(), gps.getLongitude());
//        mSocket.connect();
//        try {
//            connectToServer();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    public static class myBroadcastReceiver extends BroadcastReceiver{

        public myBroadcastReceiver() {
            super();
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.example.root.maps.call")) {

                Log.i("test", "broadcast received call");
            } else if (intent.getAction().equals("com.example.root.maps.decline")) {

                Log.i("test", "broadcast received decline");

//                    mSocket.emit("decline request" );

            }
        }
    }




}








