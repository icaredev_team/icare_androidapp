package com.example.root.maps;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.root.maps.service.AuthenticationService;
import com.example.root.maps.service.RegistrationService;
import com.example.root.maps.util.SessionManager;

public class RegisterActivity extends Activity {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    private Button btnRegister;
    private Button btnLinkToLogin;
    private EditText inputFirstlName;
    private EditText inputLastName;
    private EditText inputEmail;
    private EditText inputPassword;
    private EditText inputPhoneNumber;
    private Boolean registrationStatus;
    private SessionManager session ;
    RegistrationService registrationService ;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_layout);
        Log.i("test", "Register");

        inputFirstlName = (EditText) findViewById(R.id.firstname);
        inputLastName = (EditText) findViewById(R.id.lastname);
        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        inputPhoneNumber = (EditText) findViewById(R.id.phonenumber);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnLinkToLogin = (Button) findViewById(R.id.btnLinkToLoginScreen);

        registrationService = new RegistrationService(getApplicationContext());

        // Initialize  the Session Manager
        session = new SessionManager(getApplicationContext());

        if(session.isLoggedIn())
        {
            Intent intent = new Intent(RegisterActivity.this , NavigationActivity.class);
            startActivity(intent);
            finish();
            Toast.makeText(this, "Going to activity", Toast.LENGTH_SHORT).show();
        }


        /** Register Button Click event **/
        btnRegister.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view) {
                String firstname = inputFirstlName.getText().toString().trim();
                String lastname = inputLastName.getText().toString().trim();
                String email = inputEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();
                String phoneNumber = inputPhoneNumber.getText().toString().trim();

                if (!firstname.isEmpty() && !lastname.isEmpty() && !email.isEmpty() && !password.isEmpty() && !phoneNumber.isEmpty())
                {
                    try {
                        new RegisterTask().execute(firstname, lastname, email, password, phoneNumber);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else
                {
                    Toast.makeText(getApplicationContext(),
                            "Please enter your details!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

        // Link to Login Screen
        btnLinkToLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),
                        LoginActivity.class);
                startActivity(i);
                finish();
            }
        });

    }

    private class RegisterTask extends AsyncTask<String, Void, Boolean> {

        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
//            pDialog = new ProgressDialog(RegisterActivity.this);
//            pDialog.setMessage("Connecting.....");
//            pDialog.show();
            Log.i("test", "Here");

        }

        @Override
        protected Boolean doInBackground(String... params) {

            Log.i("test", params[0] + params[1] + params[2] + params[3] + params[4]);
            return registrationService.registerUser(params[0], params[1], params[2], params[3], params[4]);
        }

        @Override
        protected void onPostExecute(Boolean status) {
//            pDialog.dismiss();
            registrationStatus = status;
            Log.i("test", "Here2");
            if(registrationStatus) {
                /** Lunch the Login Activity **/
                Toast.makeText(getApplicationContext() , "Account Successfully Created" , Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(RegisterActivity.this , LoginActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext() , "Email Already exists!, Please try another one" , Toast.LENGTH_LONG).show();
            }
        }
    }

}