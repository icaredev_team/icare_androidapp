package com.example.root.maps.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.root.maps.model.User;

/**
 * Created by root on 28/01/17.
 */
public class DbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "userDB.db";
    public static final String TABLE_USERS = "users";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_FIRSTNAME = "firstName";
    public static final String COLUMN_LASTNAME = "lastname";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_PASSWORD = "password";
    public static final String COLUMN_PHONENUMBER = "phonenumber";
    public static final String COLUMN_DRIVER = "driver";



    public DbHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String query = "CREATE TABLE " + TABLE_USERS + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_FIRSTNAME + " TEXT , " +
                COLUMN_LASTNAME + " TEXT , " +
                COLUMN_EMAIL + " TEXT , " +
                COLUMN_PASSWORD + " TEXT , " +
                COLUMN_PHONENUMBER + " TEXT " +
                COLUMN_DRIVER + " INTEGER DEFAULT 0 " +
                ");";



        db.execSQL(query);


    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        onCreate(db);
    }

    //Add a new row to the database
    public boolean addUser(User user){

        ContentValues values = new ContentValues();
        values.put(COLUMN_FIRSTNAME, user.getFirstName());
        values.put(COLUMN_LASTNAME, user.getLastName());
        values.put(COLUMN_EMAIL, user.getEmail());
        values.put(COLUMN_PASSWORD, user.getPassword());
        values.put(COLUMN_PHONENUMBER, user.getPhoneNumber());
        if(user.isDriver())
        values.put(COLUMN_DRIVER, 1);

        SQLiteDatabase db = getWritableDatabase();

        if(db.insert(TABLE_USERS, null, values) == -1){
            db.close();
            Log.i("test", "addUser(User user) -1 ");

            return false;
        }
        Log.i("test", "addUser(User user)");
        db.close();
        return true;
    }

    //Delete a user from the database
    public void deleteUser(String email){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_USERS + " WHERE " + COLUMN_EMAIL + "=\"" + email + "\";");
    }




//    public User fetchUser(String userName){
//        User user = new User();
//
//        SQLiteDatabase db = getWritableDatabase();
//        String query = "SELECT * FROM " + TABLE_USERS + " WHERE " + COLUMN_EMAIL + "=\"" + userName + "\";";// why not leave out the WHERE  clause?
//
//        //Cursor points to a location in your results
//        Cursor recordSet = db.rawQuery(query, null);
//        //Move to the first row in your results
//        recordSet.moveToFirst();
//
//        if(!recordSet.isAfterLast()){
//
//        }
//
//        //Position after the last row means the end of the results
//        if (!recordSet.isAfterLast()) {
//            // null could happen if we used our empty constructor
//
//            user = new User(recordSet.getLong(recordSet.getColumnIndex(COLUMN_ID)),
//                    recordSet.getString(recordSet.getColumnIndex(COLUMN_NAME)),
//                    recordSet.getString(recordSet.getColumnIndex(COLUMN_LASTNAME)),
//                    recordSet.getString(recordSet.getColumnIndex(COLUMN_EMAIL)),
//                    recordSet.getString(recordSet.getColumnIndex(COLUMN_PASSWORD)),
//                    recordSet.getString(recordSet.getColumnIndex(COLUMN_PHONENUMBER))
//            );
//
////            recordSet.moveToNext();
//        }
//        else{
//            db.close();
//            return null;
//        }
//
//        db.close();
//
//
//        return user;
//    }

    public User signUp(String email, String password){

        User user ;

        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_USERS + " WHERE " + COLUMN_EMAIL + "=\"" + email + "\" AND " + COLUMN_PASSWORD + "=\"" + password + "\";" ;

        //Cursor points to a location in your results
        Cursor recordSet = db.rawQuery(query, null);
        //Move to the first row in your results
        recordSet.moveToFirst();

        //Position after the last row means the end of the results
        if (!recordSet.isAfterLast()) {

            user = new User(
                    recordSet.getString(recordSet.getColumnIndex(COLUMN_FIRSTNAME)),
                    recordSet.getString(recordSet.getColumnIndex(COLUMN_LASTNAME)),
                    recordSet.getString(recordSet.getColumnIndex(COLUMN_EMAIL)),
                    recordSet.getString(recordSet.getColumnIndex(COLUMN_PASSWORD)),
                    recordSet.getString(recordSet.getColumnIndex(COLUMN_PHONENUMBER))
            );
            if(recordSet.getInt(recordSet.getColumnIndex(COLUMN_PHONENUMBER)) == 1){
                user.setDriverTo(true);
            }


        }
        else{
            db.close();
            return null;
        }

        db.close();

        return user;


    }

    public boolean EditInfo(String firstName, String lastName, String phoneNumber, String email) {


        ContentValues values = new ContentValues();
        values.put(COLUMN_FIRSTNAME, firstName);
        values.put(COLUMN_LASTNAME, lastName);
        values.put(COLUMN_PHONENUMBER, phoneNumber);

        SQLiteDatabase db = getWritableDatabase();

        if(db.update(TABLE_USERS, values, COLUMN_EMAIL+" =\""+email+"\"", null) == -1){
            db.close();
            return false;
        }

        db.close();
        return true;
    }

    public boolean EditPassword(String password, String email) {


        ContentValues values = new ContentValues();
        values.put(COLUMN_PASSWORD, password);

        SQLiteDatabase db = getWritableDatabase();

        if(db.update(TABLE_USERS, values, COLUMN_EMAIL+" =\""+email+"\"", null) == -1){
            db.close();
            return false;
        }

        db.close();
        return true;
    }

    public User getUser(String email){
        User user = new User();



        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_USERS + " WHERE " + COLUMN_EMAIL + "=\"" + email + "\";" ;
        Cursor recordSet = db.rawQuery(query, null);

        if(recordSet.getCount() > 0) {

            recordSet.moveToFirst();
            user = new User(
                    recordSet.getString(recordSet.getColumnIndex(COLUMN_FIRSTNAME)),
                    recordSet.getString(recordSet.getColumnIndex(COLUMN_LASTNAME)),
                    recordSet.getString(recordSet.getColumnIndex(COLUMN_EMAIL)),
                    recordSet.getString(recordSet.getColumnIndex(COLUMN_PASSWORD)),
                    recordSet.getString(recordSet.getColumnIndex(COLUMN_PHONENUMBER))
            );

            if(recordSet.getInt(recordSet.getColumnIndex(COLUMN_PHONENUMBER)) == 1){
                user.setDriverTo(true);
            }


        }


        return user;
    }
}
