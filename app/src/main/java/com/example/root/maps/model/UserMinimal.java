package com.example.root.maps.model;

/**
 * Created by root on 01/02/17.
 */
public class UserMinimal {

    private long _ID;
    private String name;
    private String userName;
    private String phoneNumber;

    public UserMinimal() {
    }

    public UserMinimal(String name, String userName, String phoneNumber) {
        this.name = name;
        this.userName = userName;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
