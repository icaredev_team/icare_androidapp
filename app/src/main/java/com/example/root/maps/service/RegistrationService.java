package com.example.root.maps.service;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.example.root.maps.model.User;
import com.example.root.maps.util.DbHelper;
import com.example.root.maps.util.SessionManager;

/**
 * Created by root on 28/01/17.
 */
public class RegistrationService {


    private static DbHelper myDbHelper ;

    SessionManager session;


    public RegistrationService(Context context) {
        myDbHelper = new DbHelper(context);
        session = new SessionManager(context);
    }



    public boolean registerUser(String firstName, String lastName, String email, String password, String phoneNumber) {

        User user = new User(firstName, lastName, email, password, phoneNumber);

        boolean b = myDbHelper.addUser(user);
//        if(!b) Log.i("test", "FALSE !!");
        return b;

    }

    public boolean updateUserInfo(String firstName, String lastName, String phoneNumber){


        if(myDbHelper.EditInfo(firstName, lastName, phoneNumber, session.getUserInfo().getEmail())){

            refreshSession();
            return true;
        }
        return false;
    }

    public void refreshSession(){

        User user  = myDbHelper.getUser(session.getUserInfo().getEmail());
        session.saveUserInfo(user.getFirstName(), user.getLastName(), user.getEmail(), user.getPassword(), user.getPhoneNumber(), user.isDriver());

    }



    public boolean updateUserPassword(String password){
        return myDbHelper.EditPassword(password, session.getUserInfo().getEmail());
    }




}
