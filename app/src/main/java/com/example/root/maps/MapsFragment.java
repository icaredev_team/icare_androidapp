package com.example.root.maps;

import android.*;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.root.maps.model.Route;
import com.example.root.maps.model.UserMinimal;
import com.example.root.maps.model.User;
import com.example.root.maps.service.DirectionFinder;
import com.example.root.maps.service.LocationGpsListener;
import com.example.root.maps.util.AbsRuntimePermission;
import com.example.root.maps.util.Constant;
import com.example.root.maps.util.MapsUtil;
import com.example.root.maps.util.SessionManager;
import com.example.root.maps.util.SocketProvider;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.internal.IGoogleMapDelegate;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class MapsFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private LocationListener locListener;
    private LocationManager locManager;
    private GoogleMap mMap;
    private SessionManager session;
    private LatLng myLocation;
    private LocationGpsListener gps;
    private Socket mSocket;
    HashMap<String, Marker> hashMapMarker;
    HashMap<String, UserMinimal> drivers;
    MapsUtil mapsUtil;
    DirectionFinder directions;
    Polyline line;
    int flagPolyline = 0;
//    List<UserMinimal> drivers = new ArrayList<UserMinimal>();

    private MapFragment fragment;
    private Button btn_choose_destination;
    private Button btn_confirm_destination;
    private Button btn_abord_destination;

    protected GoogleApiClient mGoogleApiClient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SocketProvider app = (SocketProvider) getActivity().getApplication();
        mSocket = app.getSocket();
        mSocket.on("update driver location", onUpdateLocationDriver);
        mSocket.on("drivers around", onDriversAround);
        mSocket.on("driver connected", onDriverConnected);
        mSocket.on("driver disconnected", onDriverDisconnected);
        mSocket.connect();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_maps, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        session = new SessionManager(getActivity());


        if (!session.isLoggedIn()) {
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivity(intent);
        }


        fragment = (MapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        fragment.getMapAsync(this);
        btn_choose_destination = (Button) view.findViewById(R.id.btn_choose_destination);
        btn_choose_destination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(line != null){
                    line.remove();
                    if(hashMapMarker.get("mydestination")!=null)
                        mapsUtil.removeMarker("mydestination");
                }
                mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {

//
                        try {
                            directions.execute(myLocation, latLng);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                });

                Button button = (Button) view;
                button.setVisibility(View.INVISIBLE);
                button.setText("Choose destination");
                btn_abord_destination.setVisibility(view.VISIBLE);
                btn_confirm_destination.setVisibility(view.VISIBLE);

            }
        });
        btn_abord_destination = (Button) view.findViewById(R.id.btn_abord_destination);
        btn_abord_destination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.setOnMapClickListener(null);
                if(line != null){
                    line.remove();
                    mapsUtil.removeMarker("mydestination");
                }

                btn_abord_destination.setVisibility(view.INVISIBLE);
                btn_confirm_destination.setVisibility(view.INVISIBLE);
                btn_choose_destination.setVisibility(view.VISIBLE);
            }
        });
        btn_abord_destination.setVisibility(View.INVISIBLE);
        btn_confirm_destination = (Button) view.findViewById(R.id.btn_confirm_destination);
        btn_confirm_destination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.setOnMapClickListener(null);
                btn_abord_destination.setVisibility(view.INVISIBLE);
                btn_confirm_destination.setVisibility(view.INVISIBLE);
                btn_choose_destination.setText("Change destination");
                btn_choose_destination.setVisibility(view.VISIBLE);
            }
        });
        btn_confirm_destination.setVisibility(View.INVISIBLE);

        hashMapMarker = new HashMap<>();
        drivers = new HashMap<>();

        ///Class to draw polyline
        directions = new DirectionFinder() {
            @Override
            public void ApplyPolyline(Route route) {
                mMap.setOnMapClickListener(null);
                if(flagPolyline == 1){
                    line.setPoints(route.points);
                    flagPolyline = 0;
                    return;
                }
                line = mapsUtil.applyPolyline(route);
            };
        };

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == Constant.PLACE_PICKER_REQUEST){
            if(resultCode == getActivity().RESULT_OK){
                Place place = PlacePicker.getPlace(getActivity(), data);
                Log.i("test", place.getAddress().toString());
            }
        }

        android.app.FragmentManager fm = getFragmentManager();
        fm.beginTransaction().replace(R.id.fragment_container, new MapsFragment()).commit();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        Log.i("test", "onMapReady");

        mMap = googleMap;

        mapsUtil = new MapsUtil(mMap, hashMapMarker);

        mMap.setMyLocationEnabled(true);

        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                Intent callIntent = new Intent();
                callIntent.setAction("com.example.root.maps.call");
                callIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                callIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                getActivity().sendBroadcast(callIntent);
                return true;
            }
        });


//        CircleOptions circleOptions = new CircleOptions()
//                .center(new LatLng(37.4, -122.1))
//                .radius(1000); // In meters
//
//// Get back the mutable Circle
//        Circle circle = mMap.addCircle(circleOptions);


//        mapsUtil.applyPolyline(directions.getRoute());


        gps = new LocationGpsListener(getActivity(), mMap) {
            @Override
            public void onLocationChanged(Location location) {
                super.onLocationChanged(location);
                myLocation = new LatLng(gps.getLatitude(), gps.getLongitude());
                mapsUtil.UpdateLocation(myLocation, session.getUserInfo().getEmail());

                JSONObject userJSON = new JSONObject();
                JSONObject locationJSON = new JSONObject();

                try {
                    userJSON.put("category", 1);
                    userJSON.put("username", session.getUserInfo().getEmail());
                    locationJSON.put("latitude", myLocation.latitude);
                    locationJSON.put("longitude", myLocation.longitude);
                    userJSON.put("location", locationJSON);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mSocket.emit("update location", userJSON);
            }
        };

        myLocation = new LatLng(gps.getLatitude(), gps.getLongitude());

//        Log.i("test", "========" + gps.getLatitude() + "--" + gps.getLongitude());

        mapsUtil.UpdateLocation(myLocation, session.getUserInfo().getFirstName() + " " + session.getUserInfo().getLastName(), session.getUserInfo().getEmail());

        mMap.setOnMapClickListener(null);

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener()
        {
            @Override
            public void onMarkerDragStart(Marker marker)
            {
                line.setGeodesic(true);
            }

            @Override
            public void onMarkerDragEnd(Marker marker)
            {
                line.setGeodesic(false);
                mapsUtil.UpdateLocation(marker.getPosition(), "mydestination");
                try {
                    flagPolyline = 1;
                    directions.execute(myLocation, marker.getPosition());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onMarkerDrag(Marker marker)
            {
            }
        });

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

            @Override
            public void onInfoWindowClick(Marker marker) {
                // Remove the marker

                String username = "";

                JSONObject driverToAsk = new JSONObject();
                JSONObject destinationJSON = new JSONObject();
                for (Map.Entry<String, Marker> e : hashMapMarker.entrySet()) {

                    if (e.getValue().equals(marker))
                        username = e.getKey();
                }
                try {
                    driverToAsk.put("username", username);
                    if(hashMapMarker.get("mydestination") != null){
                        destinationJSON.put("latitude", hashMapMarker.get("mydestination").getPosition().latitude);
                        destinationJSON.put("longitude", hashMapMarker.get("mydestination").getPosition().latitude);
                        driverToAsk.put("destination", destinationJSON);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mSocket.emit("ask driver", driverToAsk);
            }
        });



        try {
            connectToServer();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private Emitter.Listener onDriversAround = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    JSONArray driversJSON = (JSONArray) args[0];
                    JSONObject driverJSON;
                    JSONObject locationJSON;
                    LatLng location;
                    for (int i = 0; i < driversJSON.length(); i++) {
                        try {
                            driverJSON = (JSONObject) driversJSON.getJSONObject(i);
                            drivers.put(driverJSON.getString("username"), new UserMinimal(driverJSON.getString("name"), driverJSON.getString("username"), driverJSON.getString("phonenumber")));
                            locationJSON = (JSONObject) driverJSON.getJSONObject("location");

                            location = new LatLng(locationJSON.getDouble("latitude"), locationJSON.getDouble("longitude"));
                            mapsUtil.addMarker(location, driverJSON.getString("name"), driverJSON.getString("username"));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
            });
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        mSocket.connect();
    }

    private Emitter.Listener onDriverConnected = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    JSONObject driverJSON = (JSONObject) args[0];
                    JSONObject locationJSON;
                    LatLng location;
                    try {
                        drivers.put(driverJSON.getString("username"), new UserMinimal(driverJSON.getString("name"), driverJSON.getString("username"), driverJSON.getString("phonenumber")));
                        locationJSON = (JSONObject) driverJSON.getJSONObject("location");

//                        Log.i("test", driverJSON.getString("username") + "///////////");

                        location = new LatLng(locationJSON.getDouble("latitude"), locationJSON.getDouble("longitude"));
                        mapsUtil.addMarker(location, driverJSON.getString("name"), driverJSON.getString("username"));


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            });
        }
    };

    private Emitter.Listener onUpdateLocationDriver = new Emitter.Listener() {


        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    String username;
                    JSONObject driverJSON = (JSONObject) args[0];
                    JSONObject locationJSON;
                    LatLng location;
                    try {
                        username = driverJSON.getString("username");
                        locationJSON = (JSONObject) driverJSON.getJSONObject("location");

                        location = new LatLng(locationJSON.getDouble("latitude"), locationJSON.getDouble("longitude"));
                        mapsUtil.UpdateLocation(location, username);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    };


    private Emitter.Listener onDriverDisconnected = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Log.i("test", "onDriverDisconnected");

                    JSONObject usernameJSON = (JSONObject) args[0];

                    String username;
                    try {
                        username = usernameJSON.getString("username");
                        Log.i("test", "onDriverDisconnected " + username);

                        drivers.remove(username);
                        mapsUtil.removeMarker(username);

                    } catch (JSONException e) {
                        return;
                    }

                }
            });
        }
    };


    public void connectToServer() throws JSONException {

        User u = session.getUserInfo();

        JSONObject user = new JSONObject();
        JSONObject location = new JSONObject();
        user.put("category", 1);
        user.put("name", u.getFirstName() + " " + u.getLastName());
        user.put("username", u.getEmail());
        user.put("phonenumber", u.getPhoneNumber());

        location.put("latitude", myLocation.latitude);
        location.put("longitude", myLocation.longitude);

        user.put("location", location);

        mSocket.emit("add walker", user);

    }

    @Override
    public void onStop() {
        super.onStop();
        mSocket.disconnect();
        mSocket.close();
    }

}






