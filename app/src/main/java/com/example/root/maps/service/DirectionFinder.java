package com.example.root.maps.service;

import android.os.AsyncTask;
import android.util.Log;

import com.example.root.maps.model.Route;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mai Thanh Hiep on 4/3/2016.
 */
public class DirectionFinder {
    private static final String DIRECTION_URL_API = "https://maps.googleapis.com/maps/api/directions/json?";
    private static final String GOOGLE_API_KEY = "AIzaSyDBYc98U5seLLkGfno2RypsNt4j0rdTUJw";
//    private DirectionFinderListener listener;
    private LatLng origin;
    private LatLng destination;

    public Route getRoute() {
        return route;
    }

    private Route route;

    public DirectionFinder() {
//        this.listener = listener;

        route = new Route();
    }

    public void execute(LatLng origin, LatLng destination) throws UnsupportedEncodingException {
//        listener.onDirectionFinderStart();
        this.origin = origin;
        this.destination = destination;
        new DownloadRawData().execute(createUrl());
    }

    private String createUrl() throws UnsupportedEncodingException {
//        String urlOrigin = URLEncoder.encode(origin, "utf-8");
//        String urlDestination = URLEncoder.encode(destination, "utf-8");
        Log.i("test", "Direction finder createUrl" );

        return DIRECTION_URL_API + "origin=" + origin.latitude + "," +  origin.longitude + "&destination=" + destination.latitude + "," + destination.longitude + "&key=" + GOOGLE_API_KEY;
    }

    private class DownloadRawData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            Log.i("test", "Direction finder doInBackground 1 " );

            String link = params[0];
            try {
                URL url = new URL(link);
                InputStream is = url.openConnection().getInputStream();
                StringBuffer buffer = new StringBuffer();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                Log.i("test", "Direction finder doInBackground 2" );

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                Log.i("test", "Direction finder doInBackground 3" );

                return buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("test", "Direction finder doInBackground 4mail " );

            return null;
        }

        @Override
        protected void onPostExecute(String res) {
            try {
                Log.i("test", "Direction finder onPostExecute" );

                route = parseJSon(res);
                ApplyPolyline(route);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void ApplyPolyline (Route route){

    }

    private Route parseJSon(String data) throws JSONException {
        if (data == null){
            Log.i("test", "data null !!!!" );
            return null;
        }

        Log.i("test", "data not null !!!!" );



//        Route routes ;
        JSONObject jsonData = new JSONObject(data);
        JSONArray jsonRoutes = jsonData.getJSONArray("routes");
//        for (int i = 0; i < jsonRoutes.length(); i++) {
            JSONObject jsonRoute = jsonRoutes.getJSONObject(0);
            Route route = new Route();

        Log.i("test", "data not null 222 !!!!" );


        JSONObject overview_polylineJson = jsonRoute.getJSONObject("overview_polyline");
            JSONArray jsonLegs = jsonRoute.getJSONArray("legs");
            JSONObject jsonLeg = jsonLegs.getJSONObject(0);
            JSONObject jsonDistance = jsonLeg.getJSONObject("distance");
            JSONObject jsonDuration = jsonLeg.getJSONObject("duration");
            JSONObject jsonEndLocation = jsonLeg.getJSONObject("end_location");
            JSONObject jsonStartLocation = jsonLeg.getJSONObject("start_location");

            route.distance =jsonDistance.getInt("value");
            route.duration = jsonDuration.getInt("value");
            route.endAddress = jsonLeg.getString("end_address");
            route.startAddress = jsonLeg.getString("start_address");
        Log.i("test", jsonLeg.getString("start_address") );
            route.startLocation = new LatLng(jsonStartLocation.getDouble("lat"), jsonStartLocation.getDouble("lng"));
            route.endLocation = new LatLng(jsonEndLocation.getDouble("lat"), jsonEndLocation.getDouble("lng"));
            route.points = decodePolyLine(overview_polylineJson.getString("points"));

        Log.i("test", "parseJSon(String data) " );

            return route;
//        }
    }

    private List<LatLng> decodePolyLine(final String poly) {
        int len = poly.length();
        int index = 0;
        List<LatLng> decoded = new ArrayList<LatLng>();
        int lat = 0;
        int lng = 0;

        while (index < len) {
            int b;
            int shift = 0;
            int result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            decoded.add(new LatLng(
                    lat / 100000d, lng / 100000d
            ));
        }

        return decoded;
    }
}
