package com.example.root.maps.model;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by root on 05/02/17.
 */
public class Route {
    public long distance;
    public long duration;
    public String endAddress;
    public LatLng endLocation;
    public String startAddress;
    public LatLng startLocation;

    public List<LatLng> points;

}
