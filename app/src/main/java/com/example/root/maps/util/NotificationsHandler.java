package com.example.root.maps.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.root.maps.MapsFragmentDriver;
import com.example.root.maps.R;
import com.example.root.maps.model.UserMinimal;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by root on 06/02/17.
 */
public class NotificationsHandler {
//    public void onMessageReceived(Context context ,JSONObject walker) throws JSONException {
//        //createNotification(mTitle, push_msg);
//
//        Log.e("*Notification-Response*" , walker.getString("name"));
//
//        //checkApp();
//        generateNotification(context, walker);
//
//    }


    public static void generateNotification(Context context, UserMinimal walker, String locationAdress, String destinationAdress) {



        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent checkIntent =  new Intent(context, MapsFragmentDriver.class);
        checkIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pcheckIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), checkIntent, 0);

        Intent callIntent = new Intent();
        callIntent.setAction("com.example.root.maps.call");
        callIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        callIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
//        PendingIntent pcallIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), callIntent, 0);
        PendingIntent pcallIntent = PendingIntent.getBroadcast(context, 0, callIntent, 0);

        Intent declineIntent = new Intent();
        declineIntent.setAction("com.example.root.maps.decline");
        declineIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        declineIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        PendingIntent pdeclineIntent = PendingIntent.getBroadcast(context, 0, declineIntent, 0);

        Notification notification = new NotificationCompat.Builder(context)
                .setWhen((int) System.currentTimeMillis())
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(walker.getName())
                .setContentText(locationAdress + "\nDestination :  " + destinationAdress)
//                .setContentIntent(pIntent)
//                .addAction(R.drawable.map_icon, "Check", pcheckIntent)
//                .addAction(R.drawable.call_icon, "Ok, call", pcallIntent)
//                .addAction(R.drawable.decline_icon, "Decline", pdeclineIntent)
                .addAction(0, "Check", pcheckIntent)
                .addAction(0, "Ok, call", pcallIntent)
                .addAction(0, "Decline", pdeclineIntent)
                .setAutoCancel(true)
                .build();




        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        // Play default notification sound
        notification.defaults |= Notification.DEFAULT_SOUND;

        // Vibrate if vibrate is enabled
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notificationManager.notify(999999, notification);

    }
}
