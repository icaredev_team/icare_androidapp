package com.example.root.maps;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.root.maps.model.User;
import com.example.root.maps.service.RegistrationService;
import com.example.root.maps.util.SessionManager;


public class EditInfoFragment extends Fragment {

    private static final int NEUTRAL = 0;
    private static final int BLOCK = 1;
    private static final int INSERT = 2;

    private Button btnSave;
    private Button btnCancel;
    private EditText inputFirstlName;
    private EditText inputLastName;
    private EditText inputEmail;
    private EditText inputPasswordOld;
    private EditText inputPasswordNew;
    private EditText inputPhoneNumber;
    private Boolean registrationStatus;
    RegistrationService registrationService ;
    private int passwordFlag;
    private int infoFlag;
    SessionManager session;
    private User user;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_edit_info, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        inputFirstlName = (EditText) view.findViewById(R.id.editfirstname);
        inputLastName = (EditText) view.findViewById(R.id.editlastname);
        inputEmail = (EditText) view.findViewById(R.id.editemail);
        inputPasswordOld = (EditText) view.findViewById(R.id.editpasswordold);
        inputPasswordNew = (EditText) view.findViewById(R.id.editpasswordnew);
        inputPhoneNumber = (EditText) view.findViewById(R.id.editphonenumber);
        btnSave = (Button) view.findViewById(R.id.btnSave);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);


        session = new SessionManager(getActivity());

        Log.i("test", "session");

        user = session.getUserInfo();


        inputFirstlName.setText(user.getFirstName());
        inputLastName.setText(user.getLastName());
        inputEmail.setText(user.getEmail());
        inputPhoneNumber.setText(user.getPhoneNumber());



        registrationService = new RegistrationService(getActivity().getApplicationContext());




        /** Register Button Click event **/
        btnSave.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view) {
                passwordFlag = NEUTRAL;
                infoFlag = NEUTRAL;
                String firstname = inputFirstlName.getText().toString().trim();
                String lastname = inputLastName.getText().toString().trim();
                String email = inputEmail.getText().toString().trim();
                String passwordOld = inputPasswordOld.getText().toString().trim();
                String passwordNew = inputPasswordNew.getText().toString().trim();
                String phoneNumber = inputPhoneNumber.getText().toString().trim();

                if(!passwordOld.isEmpty() || !passwordNew.isEmpty()){
                    if(!passwordOld.isEmpty() && !passwordNew.isEmpty()){
                        if(passwordOld.equals(session.getUserInfo().getPassword())){
                            if(passwordNew.equals(passwordOld)){
                                Toast.makeText(getActivity().getApplicationContext(),
                                        "Same password", Toast.LENGTH_SHORT)
                                        .show();
                                passwordFlag = BLOCK;
                            }else{
                                passwordFlag = INSERT;
                            }
                        }else {
                            Toast.makeText(getActivity().getApplicationContext(),
                                    "Wrong current password !", Toast.LENGTH_SHORT)
                                    .show();
                        }



                    }else if(!(passwordOld.isEmpty() && passwordNew.isEmpty()) && (passwordOld.isEmpty() || passwordNew.isEmpty())){
                        Toast.makeText(getActivity().getApplicationContext(),
                                "Incomplete password edition", Toast.LENGTH_SHORT)
                                .show();
                        passwordFlag = BLOCK;
                    }
                }


                if (!firstname.isEmpty() || !lastname.isEmpty() || !email.isEmpty())
                {
                    infoFlag = INSERT;
                    if(passwordFlag != BLOCK)
                    try {
                        new LoginTask().execute(firstname, lastname, phoneNumber);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else
                {
                    if(passwordFlag == INSERT)
                        try {
                            new LoginTask().execute(passwordNew);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Please enter your details!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

        // Cancel Button
        btnCancel.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                android.app.FragmentManager fm = getActivity().getFragmentManager();
                fm.beginTransaction().replace(R.id.fragment_container,  new MapsFragment()).commit();
            }
        });


    }

    private class LoginTask extends AsyncTask<String, Void, Boolean> {

        ProgressDialog pDialog;

        @Override
        protected Boolean doInBackground(String... params) {

            boolean status = true;

            Log.i("test", "doInBackground");
            if(passwordFlag == INSERT) status = status && registrationService.updateUserPassword(params[0]);
            if(infoFlag == INSERT) status = status && registrationService.updateUserInfo(params[0], params[1], params[2]);


            return status;
        }

        @Override
        protected void onPostExecute(Boolean loginStatus) {
            if (loginStatus) {
                Log.i("test", "onPostExecute");

                if(passwordFlag == INSERT  ){

                    if(infoFlag == INSERT)
                    {
                        Toast.makeText(getActivity().getApplicationContext(),
                                "Data updated succesfully", Toast.LENGTH_LONG)
                                .show();
                    }else {
                        Toast.makeText(getActivity().getApplicationContext(),
                                "Password updated succesfully", Toast.LENGTH_LONG)
                                .show();
                    }

                }else {
                    if(infoFlag == INSERT){
                        Toast.makeText(getActivity().getApplicationContext(),
                                "Data updated succesfully", Toast.LENGTH_SHORT)
                                .show();
                    }
                }

                /** Lunch the Login Activity **/



            } else {

                Toast.makeText(getActivity().getApplicationContext(),
                        "Modification failed!", Toast.LENGTH_LONG)
                        .show();
            }
        }
    }



}
