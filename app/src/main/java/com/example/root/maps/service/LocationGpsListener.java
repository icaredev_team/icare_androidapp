package com.example.root.maps.service;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.example.root.maps.R;
import com.example.root.maps.util.AbsRuntimePermission;
import com.example.root.maps.util.MapsUtil;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

public class LocationGpsListener implements LocationListener {

    public boolean isGPSEnabled = false;

    private Location location;
    private GoogleMap mMap;
    MapsUtil mapsUtil;

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 50;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 10 * 1;

    private static final int REQUEST_PERMISSION = 10;

    protected LocationManager locationManager;

    public LocationGpsListener(Activity activity, GoogleMap mMap) {
        locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        location = new Location("fake");
        location.setLatitude(0);
        location.setLongitude(0);
        this.mMap = mMap;
        mapsUtil = new MapsUtil(this.mMap);

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

    }

    public void stopLocationUpdates() {
        if (locationManager != null) {
            locationManager.removeUpdates(LocationGpsListener.this);
        }
    }


    public double getLatitude() {
        return location.getLatitude();
    }

    public double getLongitude() {
        return location.getLongitude();
    }

    public LatLng getLocation() {
        return new LatLng(location.getLatitude(), location.getLongitude());
    }


    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
    }

    @Override
    public void onProviderDisabled(String provider) {
        // NO-OP
    }

    @Override
    public void onProviderEnabled(String provider) {
        // NO-OP
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // NO-OP
    }

}