package com.example.root.maps.util;

import android.graphics.Color;
import android.util.Log;

import com.example.root.maps.model.Route;
import com.example.root.maps.model.UserMinimal;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.HashMap;

/**
 * Created by root on 04/02/17.
 */
public class MapsUtil {

    HashMap<String,Marker> hashMapMarker;
    GoogleMap mMap;


    public MapsUtil(GoogleMap mMap, HashMap<String, Marker> hashMapMarker) {
        this.hashMapMarker = hashMapMarker;
        this.mMap = mMap;
    }

    public MapsUtil(GoogleMap mMap) {
        this.mMap = mMap;
    }

    public void UpdateLocation(LatLng location, String username) {

        Marker marker = hashMapMarker.get(username);
        if(marker != null){
            marker.setPosition(location);
            hashMapMarker.put(username,marker);
            Log.i("test", "UpdateLocation");
        }

//        Log.i("test", "UpdateLocation 1111");
    }

    public Marker UpdateLocation(LatLng location, String name , String username) {

        Marker marker = hashMapMarker.get(username);
        if(marker != null){
            marker.setPosition(location);
            marker.setTitle(name);
            hashMapMarker.put(username,marker);
            return marker;
        }
        else {
            return addMarker(location, name, username);
        }
    }

    public Marker addMarker(LatLng location, String name, String username) {
        LatLng myLocation = new LatLng(location.latitude, location.longitude);
        MarkerOptions markerOptions = new MarkerOptions().position(myLocation).title(name);
        Marker marker = mMap.addMarker(markerOptions);
        if(hashMapMarker!=null)
        hashMapMarker.put(username,marker);
        return marker;
    }

    public void removeMarker(String username) {

        if(hashMapMarker.get(username) != null){
            Marker marker = hashMapMarker.get(username);
            marker.remove();
            hashMapMarker.remove(username);
        }
    }

    public Polyline applyPolyline(Route route){

        Log.i("test", route.endAddress );

        UpdateLocation(route.endLocation, route.endAddress, "mydestination").setDraggable(true);


        PolylineOptions polylineOptions = new PolylineOptions().
                geodesic(true).
                color(Color.BLUE).
                width(10);

        for (int i = 0; i < route.points.size(); i++)
            polylineOptions.add(route.points.get(i));

        return mMap.addPolyline(polylineOptions);


    }
}
