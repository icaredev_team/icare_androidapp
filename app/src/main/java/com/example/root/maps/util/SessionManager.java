package com.example.root.maps.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.View;

import com.example.root.maps.model.User;

/**
 * Created by root on 28/01/17.
 */
public class SessionManager {

    // Log Tag !
    private static String TAG = SessionManager.class.getSimpleName() ;

    // SharedPreferences
    SharedPreferences session ;
    Editor  sessionEditor ;
    Context _context ;

    // SharedPreferences Mode = 0 (PRIVATE_MODE)
    int PRIVATE_MODE = 0 ;

    // SharedPreferences file name
    private static final String sessionFileName = "etakko_user_session";

    private static final String IS_LOGGED_IN = "isLoggedIn" ;

    public SessionManager(Context context)
    {
        this._context = context ;
        session = _context.getSharedPreferences(sessionFileName , PRIVATE_MODE) ;
        sessionEditor = session.edit();
    }


    public void setLoggedIn(boolean isLoggedIn)
    {
        if(!isLoggedIn){
            sessionEditor.putString("firstName", "");
            sessionEditor.putString("lastName", "");
            sessionEditor.putString("email", "");
            sessionEditor.putString("password", "");
            sessionEditor.putString("phonenumber", "");
            sessionEditor.apply();
        }
        sessionEditor.putBoolean(IS_LOGGED_IN, isLoggedIn);
        sessionEditor.commit(); // Commit the changes !
    }


    public boolean isLoggedIn()
    {
        return session.getBoolean(IS_LOGGED_IN, false);
    }

    public void saveUserInfo(String firstName,String lastName,String email, String password, String phoneNumber, boolean isDriver){
        sessionEditor.putString("firstName", firstName);
        sessionEditor.putString("lastName", lastName);
        sessionEditor.putString("email", email);
        sessionEditor.putString("password", password);
        sessionEditor.putString("phonenumber", phoneNumber);
        sessionEditor.putBoolean("isdriver" ,isDriver);
        sessionEditor.apply();
    }

    public User getUserInfo(){
        User user = new User();

        user.setFirstName(session.getString("firstName", ""));
        user.setLastName(session.getString("lastName", ""));
        user.setEmail(session.getString("email", ""));
        user.setPassword(session.getString("password", ""));
        user.setPhoneNumber(session.getString("phonenumber", ""));
        user.setDriverTo(session.getBoolean("isdriver", false));

        return user;
    }



}
