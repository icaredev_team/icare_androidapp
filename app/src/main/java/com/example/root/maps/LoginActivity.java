package com.example.root.maps;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.example.root.maps.service.RegistrationService;
import com.example.root.maps.util.AbsRuntimePermission;
import com.example.root.maps.util.Constant;
import com.example.root.maps.util.SessionManager;
import com.example.root.maps.service.AuthenticationService;
import com.example.root.maps.util.SocketProvider;

import io.socket.client.Socket;

public class LoginActivity extends AbsRuntimePermission {
    private Button btnLogin;
    private Button btnLinkToRegister;
    private EditText inputEmail;
    private EditText inputPassword;
    private SessionManager session ;
    private final String TAG = getClass().getSimpleName();
    private AuthenticationService authenticationService;
    private Socket mSocket;

    private static final int REQUEST_PERMISSION = 10;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLinkToRegister = (Button) findViewById(R.id.btnLinkToRegisterScreen);


        // Initialize  the Session Manager

        session = new SessionManager(getApplicationContext());

        session.setLoggedIn(false);


        if(session.isLoggedIn())
        {
            requestAppPermissions(new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    R.string.msg,REQUEST_PERMISSION);
            Intent intent = new Intent(LoginActivity.this , NavigationActivity.class);
            startActivity(intent);
            finish();
        }

        Log.i("test", "Login2");

        authenticationService = new AuthenticationService(getApplicationContext());



        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                String email = inputEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                // Check for empty data in the form
                if (!email.isEmpty() && !password.isEmpty()) {

                    new LoginTask().execute(email, password);
                }
                else
                {
                    Toast.makeText(getApplicationContext(),
                            "Please enter the credentials!", Toast.LENGTH_LONG)
                            .show();
                }
            }

        });

        /** Link to Register Screen **/
        btnLinkToRegister.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),
                        RegisterActivity.class);
                startActivity(i);
                finish();
            }
        });


    }


    @Override
    public void onPermissionsGranted(int requestCode) {
        if(session.isLoggedIn()){
            Intent intent = new Intent(LoginActivity.this, NavigationActivity.class);
            startActivity(intent);
        }

    }

    private class LoginTask extends AsyncTask<String, Void, Boolean> {

        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(LoginActivity.this);
            pDialog.setMessage("Connecting.....");
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {

            return authenticationService.checkLogin(params[0], params[1]);
        }

        @Override
        protected void onPostExecute(Boolean loginStatus) {
            pDialog.dismiss();
            if (loginStatus) {
                Log.i("test", "onPostExecute");
                Toast.makeText(getApplicationContext(),
                        "logged in", Toast.LENGTH_SHORT)
                        .show();

                requestAppPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        R.string.msg, Constant.REQUEST_PERMISSION);

            } else {
                Toast.makeText(getApplicationContext(),
                        "Invalid Email Or Password, Please Try Again!", Toast.LENGTH_LONG)
                        .show();
            }
        }
    }
}